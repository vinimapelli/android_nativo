package com.example.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btsoma;
    private Button btsub;
    private Button btdiv;
    private Button btmult;

    private TextView resposta;

    private EditText Number;
    private EditText Number2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resposta = findViewById(R.id.resosta);

        Number = findViewById(R.id.number);
        Number2 = findViewById(R.id.number2);

        btsoma = findViewById(R.id.btsoma);
        btsoma.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                resposta.setText(soma(Integer.parseInt(Number.getText().toString()),Integer.parseInt(Number2.getText().toString()))+"");
            }
        });
        btsub = findViewById(R.id.btsub);
        btsub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                resposta.setText(sub(Integer.parseInt(Number.getText().toString()),Integer.parseInt(Number2.getText().toString()))+"");
            }
        });
        btmult = findViewById(R.id.btmult);
        btmult.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                resposta.setText(mult(Integer.parseInt(Number.getText().toString()),Integer.parseInt(Number2.getText().toString()))+"");
            }
        });
        btdiv = findViewById(R.id.btdiv);
        btdiv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                resposta.setText(div(Integer.parseInt(Number.getText().toString()),Integer.parseInt(Number2.getText().toString()))+"");
            }
        });


    }

    public float soma(int a, int b){
        return a+b;

    }

    public float sub(int a, int b){
        return a-b;

    }

    public float div(int a, int b){
        return a/b;

    }

    public float mult(int a, int b){
        return a*b;

    }
}
